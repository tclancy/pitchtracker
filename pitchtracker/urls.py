from django.conf.urls import patterns, url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    (r'^admin/', include(admin.site.urls)),
    url(r'^game/(?P<game_id>[^\s]+)', 'gameday.views.game_detail', name='game_detail'),
    url(r'^date/(?P<date>\d{2,}/\d{2,}/\d{4,})', 'gameday.views.game_day', name='game_day'),
    url(r'', 'gameday.views.game_day', {'date': None}, name='home'),
)

urlpatterns += staticfiles_urlpatterns()
