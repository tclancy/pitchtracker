from django.db import models
from django.db.models import Avg, Count, Max

EMPTY_PROPS = {"null": True, "blank": True}

LEAGUES = (
    ("AL", "American League"),
    ("NL", "National League")
)

class Player(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    mlb_id = models.IntegerField(**EMPTY_PROPS)
    wins = ''
    losses = ''
    era = ''

    @property
    def image_url(self):
        return "http://mlb.mlb.com/images/gameday/mugshots/mlb/%s.jpg" % self.mlb_id

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)

class Team(models.Model):
    name = models.CharField(max_length=100)
    abbreviation = models.CharField(max_length=6)
    league = models.CharField(max_length=2, blank=True, choices=LEAGUES)

    def __unicode__(self):
        return self.name

class Game(models.Model):
    date = models.DateField(db_index=True, **EMPTY_PROPS)
    start_time = models.CharField(max_length=20, blank=True)
    location = models.CharField(max_length=200, blank=True)
    home = models.ForeignKey(Team, related_name="home_games")
    away = models.ForeignKey(Team, related_name="away_games")
    mlb_id = models.CharField(max_length=255, db_index=True)

    def away_pitchers(self):
        return self._pitchers_by_side(self.away)

    def home_pitchers(self):
        return self._pitchers_by_side(self.home)
    
    def _pitchers_by_side(self, side):
        # TODO: has to be a better way to do this
        pitchers = Player.objects.filter(pk__in=
                self.at_bats.filter(pitcher_team=side, game=self).values_list(
                    "pitcher", flat=True).distinct()
            )
        for p in pitchers:
            p.average_speed = Pitch.objects.filter(at_bat__game=self,
                                    at_bat__pitcher=p).order_by().aggregate(speed=Avg('speed'))['speed']
            p.counts = Pitch.objects.filter(at_bat__game=self, at_bat__pitcher=p
                                               ).order_by().values('pitch_type').annotate(total=Count('pitch_type'))
        return pitchers

    def pitches_away(self):
        return Pitch.objects.filter(at_bat__hitter_team=self.home).order_by("at_bat", "id")

    def pitches_home(self):
        return Pitch.objects.filter(at_bat__hitter_team=self.away).order_by("at_bat", "id")

    def home_atbats(self):
        return self.hitter_atbats(self.home)

    def away_atbats(self):
        return self.hitter_atbats(self.away)

    def hitter_atbats(self, team):
        at_bats = []
        i = 0
        for ab in AtBat.objects.filter(game=self, hitter_team=team):
            if i < 9:
                at_bats.append([ab])
            else:
                at_bats[i % 9].append(ab)
            i += 1
        return at_bats

    @property
    def max_lineup_trips(self):
        result = AtBat.objects.filter(game=self).aggregate(times=Max("times_through_order"))
        if "times" in result:
            return max(1, result["times"])
        return 1

    def pitcher_stats_for_game(self, pitcher):
        pass

    def __unicode__(self):
        return "%s v. %s on %s" % (self.home.abbreviation,
                                   self.away.abbreviation, self.date.strftime("%m/%d/%Y"))

HANDS = (
    ("L", "L"),
    ("R", "R"),
)

class AtBat(models.Model):
    hitter = models.ForeignKey(Player, related_name="at_bats")
    hitter_team = models.ForeignKey(Team, related_name="hits")
    hitter_handed = models.CharField(max_length=1, choices=HANDS, db_index=True)
    pitcher = models.ForeignKey(Player, related_name="appearances")
    pitcher_team = models.ForeignKey(Team, related_name="pitches")
    game = models.ForeignKey(Game, related_name="at_bats")
    inning = models.SmallIntegerField(db_index=True)
    times_through_order = models.SmallIntegerField(db_index=True)
    result = models.CharField(max_length=2000)

    def __unicode__(self):
        return "%s hitting against %s, Inning #%d (%s)" % (self.hitter,
                                                           self.pitcher,
                                                           self.inning,
                                                           self.result)

    class Meta:
        ordering = ["id"]

class Pitch(models.Model):
    PITCH_TYPES = (
        ('FS', 'Splitter'),
        ('SL', 'Slider'),
        ('FF', '4-Seam Fastball'),
        ('FT', '2-Seam Fastball'),
        ('SI', 'Sinker'),
        ('CH', 'Change'),
        ('FA', 'Fastball'),
        ('CU', 'Curve'),
        ('FC', 'Cutter'),
        ('KN', 'Knuckle'),
        ('KC', 'Knuckle Curve'),
        ('XX', 'Unknown'),
    )
    EVENT_TYPES = (
        ('S', 'Strike'),
        ('B', 'Ball'),
        ('X', 'In Play')
    )
    at_bat = models.ForeignKey(AtBat, related_name="pitches")
    pitch_type = models.CharField(max_length=2, choices=PITCH_TYPES)
    description = models.CharField(max_length=500, blank=True)
    speed = models.FloatField(**EMPTY_PROPS)
    confidence = models.FloatField(**EMPTY_PROPS)
    nasty = models.FloatField(**EMPTY_PROPS)
    event_type = models.CharField(max_length=1, choices=EVENT_TYPES, blank=True)

    def __unicode__(self):
        return self.description

    class Meta:
        verbose_name_plural = "Pitches"
        ordering = ["at_bat", "id"]
