from django.shortcuts import render
from django.views.decorators.cache import cache_page

from .parser import GamedayParser

parser = GamedayParser()


#@cache_page(60*60)
def game_day(request, date):
    games = parser.get_games(date)
    return render(request, "index.html", {"games": games, "url": parser.current_url})


#@cache_page(5)
def game_detail(request, game_id):
    (game, home_pitchers, away_pitchers) = parser.get_game(game_id)
    if not game:
        return render(request, "no_game.html")
    return render(request, "game.html",
                  {"game": game, "url": parser.current_url,
                   "home_pitchers": home_pitchers,
                   "away_pitchers": away_pitchers,
                   "innings": range(1, 10),
                   "lineup_trips": range(1, game.max_lineup_trips + 1)})