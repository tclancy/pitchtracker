from django.contrib import admin

from .models import Player, Team, Game, AtBat, Pitch

admin.site.register(Player)
admin.site.register(Team)
admin.site.register(Game)
admin.site.register(AtBat)
admin.site.register(Pitch)
