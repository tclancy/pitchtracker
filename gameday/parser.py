import datetime
from lxml import etree
import re
import requests

from django.core.cache import cache
from .models import Pitch, Game, AtBat, Player, Team

RE_URL_MAKER = re.compile(r"/|\-")
DATE_FINDER = re.compile(r"year_(\d{4,})/month_(\d{2,})/day_(\d{2,})")

PITCH_TYPES = {k: v for (k, v) in Pitch.PITCH_TYPES}

GAME_STATUSES = {
    "In Progress": True,
    "Postponed": False,
    "Preview": False,
    "Pre-Game": True,
    "Final": True
}


class GamedayParser(object):
    """
    HACK: talking to one pitcher_data dict because I'm too lazy to find a
    better way yet
    """
    current_url = None
    pitcher_data = {}

    def __init__(self):
        self.url_base = "http://gd2.mlb.com/components/game/"

    def _get_xml(self, url):
        self.current_url = "%s%s" % (self.url_base, url)
        r = requests.get(self.current_url)
        if r.status_code == 200:
            return r
        else:
            print "IMPLEMENT LOGGING SO WE KNOW WHAT HAPPENED HERE!"
        return None

    def _get_attribute(self, node, attribute, default=""):
        return node.attrib.get(attribute, default)

    def get_games(self, date=None):
        """
        Grab games list from date's scoreboard, url format looks like
        /mlb/year_2013/month_02/day_24/playertracker.xml
        TODO: Trap date parsing errors
        """
        if date:
            date = datetime.datetime.strptime(date, "%m/%d/%Y").date()
        else:
            date = datetime.date.today()
        url_day_prefix = "mlb/%s/" % date.strftime("year_%Y/month_%m/day_%d")
        response = self._get_xml(url_day_prefix + "playertracker.xml")
        xml = etree.fromstring(response.content)
        games = []
        for node in xml.xpath("/games/game"):
            gid = node.attrib.get("id", "")
            status = self._get_attribute(node, "status")
            games.append({
                "id": gid,
                "home": self._get_attribute(node, "home_name_abbrev"),
                "away": self._get_attribute(node, "away_name_abbrev"),
                "score": "%s-%s" % (
                    self._get_attribute(node, "away_team_runs"),
                    self._get_attribute(node, "home_team_runs"),
                ),
                "status": status,
                "mlburl": "%sgid_%s" % (url_day_prefix, RE_URL_MAKER.sub("_", gid)),
                "playing": GAME_STATUSES.get(status, False),
                "over": status == "Final"
            })
        games = sorted(games, key=lambda d: (-d["playing"], d["away"]))
        return games

    def get_game(self, game_id_url):
        """
        Request XML for game info, then grab the full inning info for pitches
        """
        # TODO: Remove when we feel good about this
        Game.objects.filter(mlb_id=game_id_url).delete()
        Pitch.objects.filter(at_bat__game__mlb_id=game_id_url).delete()
        AtBat.objects.filter(game__mlb_id=game_id_url).delete()
        # END TODO
        try:
            return Game.objects.get(mlb_id=game_id_url)
        except Game.DoesNotExist:
            pass
        # reset data for this game
        game_url = "%s/game.xml" % game_id_url
        game_response = self._get_xml(game_url)
        if not game_response:
            return (None, None, None)
        xml = etree.fromstring(game_response.content)
        home = self._get_team(xml.xpath('/game/team[@type="home"]')[0])
        away = self._get_team(xml.xpath('/game/team[@type="away"]')[0])
        when = xml.xpath('/game/@game_time_et')[0]
        where = xml.xpath('/game/stadium/@name')[0]
        game = Game.objects.create(
            mlb_id=game_id_url,
            home=home,
            away=away,
            start_time=when,
            location=where,
            date=datetime.date(*[int(d) for d in DATE_FINDER.findall(game_id_url)[0]])
        )
        
        player_response = self._get_xml("%s/players.xml" % game_id_url)
        player_xml = etree.fromstring(player_response.content)

        self.get_pitching_data_for_game(game_id_url, game, player_xml, home, away)
        home_pitchers = self._get_players_stat_line(player_xml, game.home_pitchers())
        away_pitchers = self._get_players_stat_line(player_xml, game.away_pitchers())
        return [game, home_pitchers, away_pitchers]

    def _get_team(self, team_node):
        name = team_node.xpath('@name_full')[0]
        abbreviation = team_node.xpath('@abbrev')[0]
        try:
            return Team.objects.get(abbreviation=abbreviation)
        except Team.DoesNotExist:
            pass
        return Team.objects.create(
            name=name,
            abbreviation=abbreviation,
            league=team_node.xpath('@league')[0]
        )

    def get_pitching_data_for_game(self, game_id_url, game, player_xml, home, away):
        info = {}
        pitches_url = "%s/inning/inning_all.xml" % game_id_url
        pitches_response = self._get_xml(pitches_url)
        if not pitches_response:
            return info
        xml = etree.fromstring(pitches_response.content)
        self._get_pitches_for_side(xml, "top", player_xml, game, home, away)
        self._get_pitches_for_side(xml, "bottom", player_xml, game, away, home)

    def _get_player(self, player_id, player_xml, queue):
        try:
            player_id = int(player_id)
        except ValueError:
            return None
        if player_id in queue:
            return [queue, queue[player_id]]
        try:
            player = Player.objects.get(mlb_id=player_id)
            queue[player_id] = player
            return [queue, player]
        except Player.DoesNotExist:
            pass
        node = player_xml.xpath('//player[@id="%d"]' % player_id)[0]
        player = Player.objects.create(
            first_name=node.xpath('@first')[0],
            last_name=node.xpath('@last')[0],
            mlb_id=player_id
        )
        queue[player_id] = player
        return [queue, player]

    def _get_pitches_for_side(self, xml, inning_half, player_xml, game, pitching_team, batting_team):
        pitchers = {}
        hitters = {}

        times_through_order = 1
        at_bats = 1
        for atbat in xml.xpath("//%s/atbat" % inning_half):
            (pitchers, pitcher) = self._get_player(atbat.xpath("@pitcher")[0], player_xml, pitchers)
            (hitters, hitter) = self._get_player(atbat.xpath("@batter")[0], player_xml, hitters)
            ab = AtBat.objects.create(
                pitcher=pitcher,
                hitter=hitter,
                hitter_handed=atbat.xpath("@stand")[0],
                pitcher_team=pitching_team,
                hitter_team=batting_team,
                game=game,
                inning=int(atbat.xpath("../../@num")[0]),
                times_through_order=times_through_order,
                result=atbat.xpath("@des")[0]
            )
            for pitch in atbat.xpath("pitch"):
                pitch_type = self._get_attribute(pitch, "pitch_type")
                try:
                    speed = float(self._get_attribute(pitch, "start_speed"))
                except ValueError:
                    speed = None
                try:
                    confidence = float(self._get_attribute(pitch, "type_confidence"))
                except ValueError:
                    confidence = 0.0
                try:
                    nasty = float(self._get_attribute(pitch, "nasty"))
                except ValueError:
                    nasty = None
                Pitch.objects.create(
                    at_bat=ab,
                    pitch_type=pitch_type,
                    description=self._get_attribute(pitch, "des"),
                    speed=speed,
                    confidence=confidence,
                    nasty=nasty,
                    event_type=self._get_attribute(pitch, "type")
                )
            at_bats += 1
            if at_bats % 9 == 0:
                times_through_order += 1

    def _get_players_stat_line(self, player_xml, pitchers):
        """
        Get player slash line for day
        TODO: cache for a long time, but require game id for cache key
        """
        for pitcher in pitchers:
            node = player_xml.xpath('//player[@id="%d"]' % pitcher.mlb_id)[0]
            pitcher.wins = node.xpath('@wins')[0]
            pitcher.losses = node.xpath('@losses')[0]
            pitcher.era = node.xpath('@era')[0]
        return pitchers