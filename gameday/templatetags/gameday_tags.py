from django import template

from gameday.models import Pitch

register = template.Library()

PITCH_TYPES = {k: v for (k, v) in Pitch.PITCH_TYPES}

@register.filter
def pretty_pitch(pitch_type):
    if pitch_type in PITCH_TYPES:
        return PITCH_TYPES[pitch_type]
    return "Unknown"