# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Player'
        db.create_table(u'gameday_player', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('mlb_id', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'gameday', ['Player'])

        # Adding model 'Team'
        db.create_table(u'gameday_team', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(max_length=6)),
            ('league', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
        ))
        db.send_create_signal(u'gameday', ['Team'])

        # Adding model 'Game'
        db.create_table(u'gameday_game', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('date', self.gf('django.db.models.fields.DateField')(db_index=True)),
            ('home', self.gf('django.db.models.fields.related.ForeignKey')(related_name='home_games', to=orm['gameday.Team'])),
            ('away', self.gf('django.db.models.fields.related.ForeignKey')(related_name='away_games', to=orm['gameday.Team'])),
            ('mlb_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'gameday', ['Game'])

        # Adding model 'AtBat'
        db.create_table(u'gameday_atbat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hitter', self.gf('django.db.models.fields.related.ForeignKey')(related_name='at_bats', to=orm['gameday.Player'])),
            ('hitter_team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='hits', to=orm['gameday.Team'])),
            ('pitcher', self.gf('django.db.models.fields.related.ForeignKey')(related_name='appearances', to=orm['gameday.Player'])),
            ('pitcher_team', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pitches', to=orm['gameday.Team'])),
            ('game', self.gf('django.db.models.fields.related.ForeignKey')(related_name='at_bats', to=orm['gameday.Game'])),
            ('inning', self.gf('django.db.models.fields.SmallIntegerField')(db_index=True)),
            ('times_through_order', self.gf('django.db.models.fields.SmallIntegerField')(db_index=True)),
            ('result', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'gameday', ['AtBat'])

        # Adding model 'Pitch'
        db.create_table(u'gameday_pitch', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('at_bat', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pitches', to=orm['gameday.AtBat'])),
            ('pitch_type', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=500, blank=True)),
            ('speed', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('confidence', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'gameday', ['Pitch'])


    def backwards(self, orm):
        # Deleting model 'Player'
        db.delete_table(u'gameday_player')

        # Deleting model 'Team'
        db.delete_table(u'gameday_team')

        # Deleting model 'Game'
        db.delete_table(u'gameday_game')

        # Deleting model 'AtBat'
        db.delete_table(u'gameday_atbat')

        # Deleting model 'Pitch'
        db.delete_table(u'gameday_pitch')


    models = {
        u'gameday.atbat': {
            'Meta': {'object_name': 'AtBat'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'at_bats'", 'to': u"orm['gameday.Game']"}),
            'hitter': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'at_bats'", 'to': u"orm['gameday.Player']"}),
            'hitter_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'hits'", 'to': u"orm['gameday.Team']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inning': ('django.db.models.fields.SmallIntegerField', [], {'db_index': 'True'}),
            'pitcher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'appearances'", 'to': u"orm['gameday.Player']"}),
            'pitcher_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pitches'", 'to': u"orm['gameday.Team']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'times_through_order': ('django.db.models.fields.SmallIntegerField', [], {'db_index': 'True'})
        },
        u'gameday.game': {
            'Meta': {'object_name': 'Game'},
            'away': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'away_games'", 'to': u"orm['gameday.Team']"}),
            'date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'home': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'home_games'", 'to': u"orm['gameday.Team']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mlb_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'})
        },
        u'gameday.pitch': {
            'Meta': {'object_name': 'Pitch'},
            'at_bat': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pitches'", 'to': u"orm['gameday.AtBat']"}),
            'confidence': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pitch_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'speed': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        u'gameday.player': {
            'Meta': {'object_name': 'Player'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mlb_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'gameday.team': {
            'Meta': {'object_name': 'Team'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'league': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['gameday']