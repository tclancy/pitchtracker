# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Pitch.nasty'
        db.add_column(u'gameday_pitch', 'nasty',
                      self.gf('django.db.models.fields.FloatField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Pitch.nasty'
        db.delete_column(u'gameday_pitch', 'nasty')


    models = {
        u'gameday.atbat': {
            'Meta': {'object_name': 'AtBat'},
            'game': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'at_bats'", 'to': u"orm['gameday.Game']"}),
            'hitter': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'at_bats'", 'to': u"orm['gameday.Player']"}),
            'hitter_handed': ('django.db.models.fields.CharField', [], {'max_length': '1', 'db_index': 'True'}),
            'hitter_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'hits'", 'to': u"orm['gameday.Team']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inning': ('django.db.models.fields.SmallIntegerField', [], {'db_index': 'True'}),
            'pitcher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'appearances'", 'to': u"orm['gameday.Player']"}),
            'pitcher_team': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pitches'", 'to': u"orm['gameday.Team']"}),
            'result': ('django.db.models.fields.CharField', [], {'max_length': '2000'}),
            'times_through_order': ('django.db.models.fields.SmallIntegerField', [], {'db_index': 'True'})
        },
        u'gameday.game': {
            'Meta': {'object_name': 'Game'},
            'away': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'away_games'", 'to': u"orm['gameday.Team']"}),
            'date': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'home': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'home_games'", 'to': u"orm['gameday.Team']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'mlb_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'start_time': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'gameday.pitch': {
            'Meta': {'object_name': 'Pitch'},
            'at_bat': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pitches'", 'to': u"orm['gameday.AtBat']"}),
            'confidence': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nasty': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'pitch_type': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'speed': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        u'gameday.player': {
            'Meta': {'object_name': 'Player'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'mlb_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'gameday.team': {
            'Meta': {'object_name': 'Team'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'league': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['gameday']