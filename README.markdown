The idea here is to look at how pichers approach batters, how pitchers change their approach in different runs through the order, etc.
The other ideas are for me to learn Flask and learn deploying to Heroku, so things are fairly messy.

# Installation

You'll need Python installed on your machine. If you're not on Windows, you're probably all set.
Download the code and run 'pip install -r deploy/requirements.txt' from the root (reverse the slash for Windows).
If you see an error about pip not being installed, try 'easy_install pip' and then try again. If you know what you're doing,
this should all be done in a virtualenv (see http://www.clemesha.org/blog/modern-python-hacker-tools-virtualenv-fabric-pip/ for more on
virtualenv and pip).

# Using
The front page shows a list of all games the MLB site knows about for today. Clicking a link will provide game details and the home and away
pitch sequences, assuming the game has started. You can also view other days by going to /date/month/day/year/ (e.g., "/date/09/27/2011/").

# Tests

There aren't any yet. There really should be. There's a /sample_data folder of Gameday data stolen from the Ruby Gameday API.
The parser needs to be updated to accept XML or just read the data in and pass it to the parser directly.

# References

* Ruby Gameday API: https://github.com/timothyf/gameday_api
* http://gd2.mlb.com/components/game/
* http://fastballs.wordpress.com/2007/08/02/glossary-of-the-gameday-pitch-fields/