from __future__ import with_statement

import os
from time import sleep

from fabric.api import env, local, run, cd, get
from pitchtracker.live_settings import DATABASES as DB

MAX_RESTART_ATTEMPTS = 3

env.hosts = ['tkc.webfactional.com:22']
APP_PATH = '/home/tkc/webapps/localnine/pitches'
env.apache_path = '/home/tkc/webapps/localnine/apache2/bin'
env.user = 'tkc'
env.password = os.getenv('TKC_PASSWORD')


def test():
    local('python manage.py test', capture=False)


def deploy():
    with cd(APP_PATH):
        run('git pull')
        run('pip-2.7 install -r requirements.txt')
        run('python2.7 manage.py collectstatic --noinput')
        syncdb()
    restart()


def dump_db():
    #dump_pg_db()
    pass


def dump_pg_db():
    db = DB['default']
    run('PGOPTIONS="-c statement_timeout=0" pg_dump -U %s %s > /home/%s/databackups/%s.backup'
            % (db['USER'], db['NAME'], env.user, db['NAME']))


def get_database_path():
    return '/home/%s/databackups/%s.dump' % (env.user, env.user)


def dldb():
    dump_db()
    home = os.getenv('USERPROFILE') or os.getenv('HOME')
    download_path = os.path.join(home, 'Desktop', '%s.sql' % env.user)
    get(get_database_path(), download_path)


def syncdb():
    dump_db()
    with cd(APP_PATH):
        run('/usr/local/bin/python2.7 manage.py syncdb')
        run('/usr/local/bin/python2.7 manage.py migrate --all')
    restart()


def restart(count=0):
    with cd(env.apache_path):
        run('./stop')
        sleep(1)
        cmd = run('./start')
        if cmd.find('already running') > -1:
            print 'RESTART FAILURE!'
            if count < MAX_RESTART_ATTEMPTS:
                print 'Retrying . . . '
                count = count + 1
                sleep(3 * count)
                restart(count)
        else:
            print 'Restart succeeded'
